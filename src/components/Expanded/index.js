import React, {useState} from 'react';
import './index.css';

export const Expanded = () => (
    <div className="expanded-element"> 
    <hr id="separator"/>
    <div id="first-row">To contact tech team, please send an email to <span id="email">tech@make.org</span> </div>
    <div id="second-row"> We will respond as soon as possible except if your request is about a bug.</div>   
     </div>
    );
 

