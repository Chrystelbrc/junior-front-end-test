import React, {useState} from 'react';
import {Expanded} from '../Expanded';
import arrow1 from './arrow1.svg';
import './index.css';

export const Collapse = () => {
    const [expand, setExpand] = useState(false);
    const handleClick = () => setExpand(!expand);
    return (
        <div>
            <div className="collapsed-element">  
    <button onClick={handleClick} className="btn"> {expand ? "Click to collapse" : "Click to expand" }</button>
    <img onClick={handleClick}src={arrow1} style={{ transform: `rotate(${expand ? 180 : 0}deg)` }} className="svg" />
    </div>
    {expand && <Expanded/> }
    </div>
    )
} 
