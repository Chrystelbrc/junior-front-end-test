import React, { useState } from 'react';

export const Calculator = () => {
    const [firstValue,setFirstValue] = useState(0);
    const [secondValue,setSecondValue] = useState(0);
    const [result,setResult] = useState();

    const updateFirstValue = event => {
        setFirstValue(parseInt(event.target.value))
    }

    const updateSecondValue = event => {
        setSecondValue(parseInt(event.target.value))
    }

    return(
        <>
            <div>
                <label className="block-label">
                    <span className="label-title">firstValue</span>
                    <input type="number" value={firstValue} name="firstValue" onChange={updateFirstValue}/>
                </label>
                <label className="block-label">
                    <span className="label-title">secondValue</span>
                    <input type="number" value={secondValue} name="secondValue" onChange={updateSecondValue} />
                </label>
                <button className="calculate-buttons" onClick={() => setResult(firstValue + secondValue)}>
                    Add
                </button>
                <button className="calculate-buttons" onClick={() => setResult(firstValue - secondValue)}>
                    Substract
                </button>
                <button className="calculate-buttons" onClick={() => setResult(firstValue * secondValue)}>
                    Multiply
                </button>
                <button className="calculate-buttons" onClick={() => setResult(firstValue / secondValue)}>
                    Divide
                </button>
            </div>
            <p>Result is : {result}</p>
        </>
    );
}