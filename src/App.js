import React from 'react';
import './App.css';
import './components/Calculator/index.css';
import { TestDescription } from './components/Descriptions/TestDescription';
import { CalculatorDescription } from './components/Descriptions/CalculatorDescription';
import { Calculator } from './components/Calculator';
import { CollapseDescription } from './components/Descriptions/CollapseDescription';
import { Collapse } from './components/Collapse';
import { Expanded } from './components/Expanded';

function App() {
  return (
    <div className="app">
      <TestDescription />
      <section>
        <CalculatorDescription />
        <div className="app-exercice ">
            <h3 className="app-exercise-title">Exercice to completed :</h3>
            <Calculator />
        </div>
      </section>
      <section>
        <CollapseDescription />
        <div className="app-exercice ">
            <h3 className="app-exercise-title">Exercice to completed :</h3>
            <Collapse />
        </div>
      </section>
    </div>
  );
}

export default App;
